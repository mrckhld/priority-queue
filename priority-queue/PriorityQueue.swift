//
//  main.swift
//  priority-queue
//
//  Created by Michael Rockhold on 8/24/20.
//  Copyright © 2020 Michael Rockhold. All rights reserved.
//

import Foundation

protocol PriorityQueue {
    associatedtype KeyType
    
    func insert(key: KeyType) -> Void
        
    func popMax() -> KeyType?
    
    func isEmpty() -> Bool
    
    func size() -> Int
}


class HeapPriorityQueue: PriorityQueue {

    typealias KeyType = Int
    let comparator: (KeyType, KeyType)->Bool
    private var pq = [KeyType()]
    
    init(comparator: @escaping (KeyType, KeyType)->Bool) {
        self.comparator = comparator
    }
    
    func less(_ idx1: Int, _ idx2: Int) -> Bool {
        return self.comparator(pq[idx1], pq[idx2])
    }

    func insert(key: KeyType) {
        pq.append(key)
        swim(size())
    }
        
    func popMax() -> KeyType? {
        if pq.count == 1 {
            return nil
        }
        let max = pq[1]
        exch(1, size())
        pq.removeLast()
        sink(1)
        return max
    }
    
    func isEmpty() -> Bool {
        return size() == 0
    }
    
    func size() -> Int {
        return pq.count - 1
    }
    
    private func swim(_ idx: Int) {
        var k = idx
        while k > 1 && less(k/2, k) {
            exch(k/2, k)
            k = k / 2
        }
    }
    
    private func sink(_ idx: Int) {
        var k = idx
        while 2 * k <= size() {
            var j = 2 * k
            if j < size() && less(j, j+1) {
                j = j + 1
            }
            if !less(k, j) {
                break
            }
            exch(k, j)
            k = j
        }
    }
    
    private func exch(_ i: Int, _ j: Int) {
        let temp = pq[i]
        pq[i] = pq[j]
        pq[j] = temp
    }
}
