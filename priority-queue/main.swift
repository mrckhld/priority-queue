//
//  main.swift
//  priority-queue
//
//  Created by Michael Rockhold on 8/24/20.
//  Copyright © 2020 Michael Rockhold. All rights reserved.
//

import Foundation

var hpq = HeapPriorityQueue { (a, b) -> Bool in
    return a > b
}

hpq.insert(key: 87)
hpq.insert(key: 3)
hpq.insert(key: 51)
hpq.insert(key: 83)
hpq.insert(key: 11)
hpq.insert(key: 37)
hpq.insert(key: -7)
hpq.insert(key: 99)

while let v = hpq.popMax() {

    print(v)
    
}
